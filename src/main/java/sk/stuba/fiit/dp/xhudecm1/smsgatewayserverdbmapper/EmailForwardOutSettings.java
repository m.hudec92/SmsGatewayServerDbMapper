package sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper;
// Generated Apr 28, 2017 12:20:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EmailForwardOutSettings generated by hbm2java
 */
@Entity
@Table(name="email_forward_out_settings"
    ,schema="public"
)
public class EmailForwardOutSettings  implements java.io.Serializable {


     private Long id;
     private ClientSecureCommunicationData clientSecureCommunicationData;
     private String destinationEmailAddress;
     private String subject;
     private String body;
     private Date createdAt;
     private Date modifiedAt;
     private String description;
     private String activeStatus;
     private Set<EmailSent> emailSents = new HashSet<EmailSent>(0);

    public EmailForwardOutSettings() {
    }

    public EmailForwardOutSettings(ClientSecureCommunicationData clientSecureCommunicationData, String destinationEmailAddress, String subject, String body, Date createdAt, Date modifiedAt, String description, String activeStatus, Set<EmailSent> emailSents) {
       this.clientSecureCommunicationData = clientSecureCommunicationData;
       this.destinationEmailAddress = destinationEmailAddress;
       this.subject = subject;
       this.body = body;
       this.createdAt = createdAt;
       this.modifiedAt = modifiedAt;
       this.description = description;
       this.activeStatus = activeStatus;
       this.emailSents = emailSents;
    }
   
     @SequenceGenerator(name="generator", sequenceName="email_forward_out_settings_id_seq")@Id @GeneratedValue(strategy=SEQUENCE, generator="generator")

    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="client_id")
    public ClientSecureCommunicationData getClientSecureCommunicationData() {
        return this.clientSecureCommunicationData;
    }
    
    public void setClientSecureCommunicationData(ClientSecureCommunicationData clientSecureCommunicationData) {
        this.clientSecureCommunicationData = clientSecureCommunicationData;
    }

    
    @Column(name="destination_email_address")
    public String getDestinationEmailAddress() {
        return this.destinationEmailAddress;
    }
    
    public void setDestinationEmailAddress(String destinationEmailAddress) {
        this.destinationEmailAddress = destinationEmailAddress;
    }

    
    @Column(name="subject")
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="body")
    public String getBody() {
        return this.body;
    }
    
    public void setBody(String body) {
        this.body = body;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", length=29)
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_at", length=29)
    public Date getModifiedAt() {
        return this.modifiedAt;
    }
    
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="active_status")
    public String getActiveStatus() {
        return this.activeStatus;
    }
    
    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="emailForwardOutSettings")
    public Set<EmailSent> getEmailSents() {
        return this.emailSents;
    }
    
    public void setEmailSents(Set<EmailSent> emailSents) {
        this.emailSents = emailSents;
    }




}


