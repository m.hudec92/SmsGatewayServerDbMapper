package sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper;
// Generated Apr 28, 2017 12:20:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * RegisteredUser generated by hbm2java
 */
@Entity
@Table(name="registered_user"
    ,schema="public"
    , uniqueConstraints = @UniqueConstraint(columnNames="email") 
)
public class RegisteredUser  implements java.io.Serializable {


     private Long id;
     private String userName;
     private Date createdAt;
     private Date modifiedAt;
     private String email;
     private String passwordHash;
     private String passwordSalt;
     private String userUuid;
     private Set<MobileNumbers> mobileNumberses = new HashSet<MobileNumbers>(0);
     private Set<EmailForwardInSettings> emailForwardInSettingses = new HashSet<EmailForwardInSettings>(0);
     private Set<ClientSecureCommunicationDataHistory> clientSecureCommunicationDataHistories = new HashSet<ClientSecureCommunicationDataHistory>(0);
     private Set<WsForwardInSettings> wsForwardInSettingses = new HashSet<WsForwardInSettings>(0);
     private Set<ClientSecureCommunicationData> clientSecureCommunicationDatas = new HashSet<ClientSecureCommunicationData>(0);

    public RegisteredUser() {
    }

    public RegisteredUser(String userName, Date createdAt, Date modifiedAt, String email, String passwordHash, String passwordSalt, String userUuid, Set<MobileNumbers> mobileNumberses, Set<EmailForwardInSettings> emailForwardInSettingses, Set<ClientSecureCommunicationDataHistory> clientSecureCommunicationDataHistories, Set<WsForwardInSettings> wsForwardInSettingses, Set<ClientSecureCommunicationData> clientSecureCommunicationDatas) {
       this.userName = userName;
       this.createdAt = createdAt;
       this.modifiedAt = modifiedAt;
       this.email = email;
       this.passwordHash = passwordHash;
       this.passwordSalt = passwordSalt;
       this.userUuid = userUuid;
       this.mobileNumberses = mobileNumberses;
       this.emailForwardInSettingses = emailForwardInSettingses;
       this.clientSecureCommunicationDataHistories = clientSecureCommunicationDataHistories;
       this.wsForwardInSettingses = wsForwardInSettingses;
       this.clientSecureCommunicationDatas = clientSecureCommunicationDatas;
    }
   
     @SequenceGenerator(name="generator", sequenceName="registered_user_id_seq")@Id @GeneratedValue(strategy=SEQUENCE, generator="generator")

    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    
    @Column(name="user_name")
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", length=29)
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_at", length=29)
    public Date getModifiedAt() {
        return this.modifiedAt;
    }
    
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    
    @Column(name="email", unique=true)
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    
    @Column(name="password_hash")
    public String getPasswordHash() {
        return this.passwordHash;
    }
    
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    
    @Column(name="password_salt")
    public String getPasswordSalt() {
        return this.passwordSalt;
    }
    
    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    
    @Column(name="user_uuid")
    public String getUserUuid() {
        return this.userUuid;
    }
    
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="registeredUser")
    public Set<MobileNumbers> getMobileNumberses() {
        return this.mobileNumberses;
    }
    
    public void setMobileNumberses(Set<MobileNumbers> mobileNumberses) {
        this.mobileNumberses = mobileNumberses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="registeredUser")
    public Set<EmailForwardInSettings> getEmailForwardInSettingses() {
        return this.emailForwardInSettingses;
    }
    
    public void setEmailForwardInSettingses(Set<EmailForwardInSettings> emailForwardInSettingses) {
        this.emailForwardInSettingses = emailForwardInSettingses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="registeredUser")
    public Set<ClientSecureCommunicationDataHistory> getClientSecureCommunicationDataHistories() {
        return this.clientSecureCommunicationDataHistories;
    }
    
    public void setClientSecureCommunicationDataHistories(Set<ClientSecureCommunicationDataHistory> clientSecureCommunicationDataHistories) {
        this.clientSecureCommunicationDataHistories = clientSecureCommunicationDataHistories;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="registeredUser")
    public Set<WsForwardInSettings> getWsForwardInSettingses() {
        return this.wsForwardInSettingses;
    }
    
    public void setWsForwardInSettingses(Set<WsForwardInSettings> wsForwardInSettingses) {
        this.wsForwardInSettingses = wsForwardInSettingses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="registeredUser")
    public Set<ClientSecureCommunicationData> getClientSecureCommunicationDatas() {
        return this.clientSecureCommunicationDatas;
    }
    
    public void setClientSecureCommunicationDatas(Set<ClientSecureCommunicationData> clientSecureCommunicationDatas) {
        this.clientSecureCommunicationDatas = clientSecureCommunicationDatas;
    }




}


