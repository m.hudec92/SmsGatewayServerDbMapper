package sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper;
// Generated Apr 28, 2017 12:20:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EmailForwardInSettings generated by hbm2java
 */
@Entity
@Table(name="email_forward_in_settings"
    ,schema="public"
)
public class EmailForwardInSettings  implements java.io.Serializable {


     private Long id;
     private RegisteredUser registeredUser;
     private String sourceEmailAddress;
     private String subject;
     private String body;
     private String dataType;
     private Date createdAt;
     private Date modifiedAt;
     private String emailIdentification;
     private String description;
     private String activeStatus;
     private Set<EmailReceived> emailReceiveds = new HashSet<EmailReceived>(0);

    public EmailForwardInSettings() {
    }

    public EmailForwardInSettings(RegisteredUser registeredUser, String sourceEmailAddress, String subject, String body, String dataType, Date createdAt, Date modifiedAt, String emailIdentification, String description, String activeStatus, Set<EmailReceived> emailReceiveds) {
       this.registeredUser = registeredUser;
       this.sourceEmailAddress = sourceEmailAddress;
       this.subject = subject;
       this.body = body;
       this.dataType = dataType;
       this.createdAt = createdAt;
       this.modifiedAt = modifiedAt;
       this.emailIdentification = emailIdentification;
       this.description = description;
       this.activeStatus = activeStatus;
       this.emailReceiveds = emailReceiveds;
    }
   
     @SequenceGenerator(name="generator", sequenceName="email_forward_in_settings_id_seq")@Id @GeneratedValue(strategy=SEQUENCE, generator="generator")

    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id")
    public RegisteredUser getRegisteredUser() {
        return this.registeredUser;
    }
    
    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }

    
    @Column(name="source_email_address")
    public String getSourceEmailAddress() {
        return this.sourceEmailAddress;
    }
    
    public void setSourceEmailAddress(String sourceEmailAddress) {
        this.sourceEmailAddress = sourceEmailAddress;
    }

    
    @Column(name="subject")
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="body")
    public String getBody() {
        return this.body;
    }
    
    public void setBody(String body) {
        this.body = body;
    }

    
    @Column(name="data_type")
    public String getDataType() {
        return this.dataType;
    }
    
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", length=29)
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_at", length=29)
    public Date getModifiedAt() {
        return this.modifiedAt;
    }
    
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    
    @Column(name="email_identification")
    public String getEmailIdentification() {
        return this.emailIdentification;
    }
    
    public void setEmailIdentification(String emailIdentification) {
        this.emailIdentification = emailIdentification;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="active_status")
    public String getActiveStatus() {
        return this.activeStatus;
    }
    
    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="emailForwardInSettings")
    public Set<EmailReceived> getEmailReceiveds() {
        return this.emailReceiveds;
    }
    
    public void setEmailReceiveds(Set<EmailReceived> emailReceiveds) {
        this.emailReceiveds = emailReceiveds;
    }




}


