package sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper;
// Generated Apr 28, 2017 12:20:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * WsForwardInSettings generated by hbm2java
 */
@Entity
@Table(name="ws_forward_in_settings"
    ,schema="public"
)
public class WsForwardInSettings  implements java.io.Serializable {


     private Long id;
     private RegisteredUser registeredUser;
     private String sourceIpAddress;
     private Date createdAt;
     private Date modifiedAt;
     private String wsAuthName;
     private String wsAuthPassword;
     private String wsIdentification;
     private String description;
     private String activeStatus;
     private Set<WsReceived> wsReceiveds = new HashSet<WsReceived>(0);

    public WsForwardInSettings() {
    }

    public WsForwardInSettings(RegisteredUser registeredUser, String sourceIpAddress, Date createdAt, Date modifiedAt, String wsAuthName, String wsAuthPassword, String wsIdentification, String description, String activeStatus, Set<WsReceived> wsReceiveds) {
       this.registeredUser = registeredUser;
       this.sourceIpAddress = sourceIpAddress;
       this.createdAt = createdAt;
       this.modifiedAt = modifiedAt;
       this.wsAuthName = wsAuthName;
       this.wsAuthPassword = wsAuthPassword;
       this.wsIdentification = wsIdentification;
       this.description = description;
       this.activeStatus = activeStatus;
       this.wsReceiveds = wsReceiveds;
    }
   
     @SequenceGenerator(name="generator", sequenceName="ws_forward_in_settings_id_seq")@Id @GeneratedValue(strategy=SEQUENCE, generator="generator")

    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id")
    public RegisteredUser getRegisteredUser() {
        return this.registeredUser;
    }
    
    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }

    
    @Column(name="source_ip_address")
    public String getSourceIpAddress() {
        return this.sourceIpAddress;
    }
    
    public void setSourceIpAddress(String sourceIpAddress) {
        this.sourceIpAddress = sourceIpAddress;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", length=29)
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_at", length=29)
    public Date getModifiedAt() {
        return this.modifiedAt;
    }
    
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    
    @Column(name="ws_auth_name")
    public String getWsAuthName() {
        return this.wsAuthName;
    }
    
    public void setWsAuthName(String wsAuthName) {
        this.wsAuthName = wsAuthName;
    }

    
    @Column(name="ws_auth_password")
    public String getWsAuthPassword() {
        return this.wsAuthPassword;
    }
    
    public void setWsAuthPassword(String wsAuthPassword) {
        this.wsAuthPassword = wsAuthPassword;
    }

    
    @Column(name="ws_identification")
    public String getWsIdentification() {
        return this.wsIdentification;
    }
    
    public void setWsIdentification(String wsIdentification) {
        this.wsIdentification = wsIdentification;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="active_status")
    public String getActiveStatus() {
        return this.activeStatus;
    }
    
    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="wsForwardInSettings")
    public Set<WsReceived> getWsReceiveds() {
        return this.wsReceiveds;
    }
    
    public void setWsReceiveds(Set<WsReceived> wsReceiveds) {
        this.wsReceiveds = wsReceiveds;
    }




}


