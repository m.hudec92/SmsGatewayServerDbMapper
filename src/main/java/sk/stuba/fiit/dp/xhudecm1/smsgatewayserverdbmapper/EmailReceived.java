package sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper;
// Generated Apr 28, 2017 12:20:54 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EmailReceived generated by hbm2java
 */
@Entity
@Table(name="email_received"
    ,schema="public"
)
public class EmailReceived  implements java.io.Serializable {


     private Long id;
     private EmailForwardInSettings emailForwardInSettings;
     private String sourceMailAddress;
     private String subject;
     private String data;
     private Date createdAt;
     private String emailId;
     private String emailThreadId;
     private String emailSnippet;
     private Long emailHistoryId;
     private Long emailInternalDate;
     private String emailReceivedStatusType;
     private Set<TemporarySmsStorage> temporarySmsStorages = new HashSet<TemporarySmsStorage>(0);
     private Set<SmsSent> smsSents = new HashSet<SmsSent>(0);

    public EmailReceived() {
    }

    public EmailReceived(EmailForwardInSettings emailForwardInSettings, String sourceMailAddress, String subject, String data, Date createdAt, String emailId, String emailThreadId, String emailSnippet, Long emailHistoryId, Long emailInternalDate, String emailReceivedStatusType, Set<TemporarySmsStorage> temporarySmsStorages, Set<SmsSent> smsSents) {
       this.emailForwardInSettings = emailForwardInSettings;
       this.sourceMailAddress = sourceMailAddress;
       this.subject = subject;
       this.data = data;
       this.createdAt = createdAt;
       this.emailId = emailId;
       this.emailThreadId = emailThreadId;
       this.emailSnippet = emailSnippet;
       this.emailHistoryId = emailHistoryId;
       this.emailInternalDate = emailInternalDate;
       this.emailReceivedStatusType = emailReceivedStatusType;
       this.temporarySmsStorages = temporarySmsStorages;
       this.smsSents = smsSents;
    }
   
     @SequenceGenerator(name="generator", sequenceName="email_received_id_seq")@Id @GeneratedValue(strategy=SEQUENCE, generator="generator")

    
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="email_forward_in_id")
    public EmailForwardInSettings getEmailForwardInSettings() {
        return this.emailForwardInSettings;
    }
    
    public void setEmailForwardInSettings(EmailForwardInSettings emailForwardInSettings) {
        this.emailForwardInSettings = emailForwardInSettings;
    }

    
    @Column(name="source_mail_address")
    public String getSourceMailAddress() {
        return this.sourceMailAddress;
    }
    
    public void setSourceMailAddress(String sourceMailAddress) {
        this.sourceMailAddress = sourceMailAddress;
    }

    
    @Column(name="subject")
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="data")
    public String getData() {
        return this.data;
    }
    
    public void setData(String data) {
        this.data = data;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", length=29)
    public Date getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    
    @Column(name="email_id")
    public String getEmailId() {
        return this.emailId;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    
    @Column(name="email_thread_id")
    public String getEmailThreadId() {
        return this.emailThreadId;
    }
    
    public void setEmailThreadId(String emailThreadId) {
        this.emailThreadId = emailThreadId;
    }

    
    @Column(name="email_snippet")
    public String getEmailSnippet() {
        return this.emailSnippet;
    }
    
    public void setEmailSnippet(String emailSnippet) {
        this.emailSnippet = emailSnippet;
    }

    
    @Column(name="email_history_id")
    public Long getEmailHistoryId() {
        return this.emailHistoryId;
    }
    
    public void setEmailHistoryId(Long emailHistoryId) {
        this.emailHistoryId = emailHistoryId;
    }

    
    @Column(name="email_internal_date")
    public Long getEmailInternalDate() {
        return this.emailInternalDate;
    }
    
    public void setEmailInternalDate(Long emailInternalDate) {
        this.emailInternalDate = emailInternalDate;
    }

    
    @Column(name="email_received_status_type")
    public String getEmailReceivedStatusType() {
        return this.emailReceivedStatusType;
    }
    
    public void setEmailReceivedStatusType(String emailReceivedStatusType) {
        this.emailReceivedStatusType = emailReceivedStatusType;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="emailReceived")
    public Set<TemporarySmsStorage> getTemporarySmsStorages() {
        return this.temporarySmsStorages;
    }
    
    public void setTemporarySmsStorages(Set<TemporarySmsStorage> temporarySmsStorages) {
        this.temporarySmsStorages = temporarySmsStorages;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="emailReceived")
    public Set<SmsSent> getSmsSents() {
        return this.smsSents;
    }
    
    public void setSmsSents(Set<SmsSent> smsSents) {
        this.smsSents = smsSents;
    }




}


